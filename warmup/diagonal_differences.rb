#!/bin/ruby

n = gets.strip.to_i
a = Array.new(n)
for a_i in (0..n-1)
  a_t = gets.strip
  a[a_i] = a_t.split(' ').map(&:to_i)
end

def leftDiagonalPosition?(i,j)
  i == j
end
def rightDiagonalPosition?(i,j,n)
  i + j == n-1
end


def diagonal_difference(arr,n)
  leftDiagonalSum = 0
  rightDiagonalSum= 0
  for i in (0..n-1)
    for j in (0..n-1)
      if leftDiagonalPosition?(i,j)
        leftDiagonalSum += arr[i][j]
      end
      if rightDiagonalPosition?(i,j,n)
        rightDiagonalSum +=arr[i][j]
      end
    end
  end
  (leftDiagonalSum-rightDiagonalSum).abs
end

p diagonal_difference(a,n)