#!/bin/ruby

def arraySumWithoutElementAt(arr,withoutElementAt)
  sum = 0
  for i in (0..arr.length-1)
    if (i != withoutElementAt)
      sum = sum + arr[i]
    end
  end
  sum
end

def minimaxSum(arr)
  sumResult = []
  for i in (0..arr.length-1)
    sumResult[i] = arraySumWithoutElementAt(arr,i)
  end
  [sumResult.min,sumResult.max]
end

arr = gets.strip
arr = arr.split(' ').map(&:to_i)
print minimaxSum(arr).join(" ")
