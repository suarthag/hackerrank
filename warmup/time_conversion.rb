#!/bin/ruby

def timeConversion(s)
  # Complete this function
  hour = s[0..1]
  ampm = s[8..9]
  if hour == "12" && ampm=="AM"
    militaryHour = 0
  elsif ampm=="PM"
    militaryHour = ((hour.to_i%12)+12)%24
  else
    militaryHour = hour
  end
  "#{militaryHour.to_s.rjust(2,'0')}#{s[2..7]}"
end

s = gets.strip
result = timeConversion(s)
puts result

