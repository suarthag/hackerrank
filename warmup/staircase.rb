#!/bin/ruby

n = gets.strip.to_i

for i in (1..n)
  for j in (1..n)
    if j<=n-i
      print " "
    elsif j>n-i
      print '#'
    end
  end
  puts
end