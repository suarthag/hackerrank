#!/bin/ruby

def plus_minus_fraction(n,arr)
  positiveCount = 0.0
  negativeCount = 0.0
  zeroCount = 0.0
  arr.each do |number|
    case
      when number > 0
        positiveCount += 1
      when number < 0
        negativeCount += 1
      when number == 0
        zeroCount += 1
    end
  end
  positiveFraction = getFraction(positiveCount,n)
  negativeFraction = getFraction(negativeCount,n)
  zeroFraction = getFraction(zeroCount,n)
  
  [positiveFraction,negativeFraction,zeroFraction]
end

def getFraction(numerator,denominator)
  sprintf('%.6f', (numerator/denominator).round(6))
end

n = gets.strip.to_i
arr = gets.strip
arr = arr.split(' ').map(&:to_i)

puts plus_minus_fraction(n,arr).join("\n")