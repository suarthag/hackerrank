#!/bin/ruby

def solve(a0, a1, a2, b0, b1, b2)
  res = [0,0]
  resultA = a0>b0 ? 1 : 0
  resultA += a1>b1 ? 1 : 0
  resultA += a2>b2 ? 1 : 0

  resultb = a0<b0 ? 1 : 0
  resultb += a1<b1 ? 1 : 0
  resultb += a2<b2 ? 1 : 0

  [resultA.to_s,resultb.to_s]

end

a0, a1, a2 = gets.strip.split(' ')
a0 = a0.to_i
a1 = a1.to_i
a2 = a2.to_i
b0, b1, b2 = gets.strip.split(' ')
b0 = b0.to_i
b1 = b1.to_i
b2 = b2.to_i
result = solve(a0, a1, a2, b0, b1, b2)
print result.join(" ")



