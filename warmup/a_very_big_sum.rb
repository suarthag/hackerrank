#!/bin/ruby

def aVeryBigSum(n, ar)
  # Complete this function
  sum = 0
  ar.each do |number|
    sum = sum + number
  end
  sum
end

n = gets.strip.to_i
ar = gets.strip
ar = ar.split(' ').map(&:to_i)
result = aVeryBigSum(n, ar)
puts result;

