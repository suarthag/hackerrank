#!/bin/ruby


def separate_number(s)
  max_possible_length = s.length/2
  result = -1
  for i in (1..max_possible_length)
    first_number = s[0..i-1].to_i
    second_number_string = (first_number+1).to_s
    if s[i..(i+second_number_string.length)-1]==second_number_string
      if is_beautiful?(s,first_number)
        result = first_number
      end
    end
  end
  if result > 0
    puts "YES #{result}"
  else
    puts "NO"
  end
end

def is_beautiful?(s,first_number)
  index = 0
  check_number = first_number
  beautiful = false
  while index<s.length
    if s[index..(index+check_number.to_s.length)-1] == check_number.to_s
      if (index+check_number.to_s.length)== s.length
        return true
      else
        index = index+check_number.to_s.length
        check_number+=1
      end
    else
      index = s.length
      beautiful = false
    end
  end
  beautiful
end

q = gets.strip.to_i
for a0 in (0..q-1)
  s = gets.strip
  separate_number(s)
end

