#!/bin/ruby

def funnyString(s)
  r = s.reverse
  ctr=0
  for i in (0..s.length-2)
    if (s[i+1].ord-s[i].ord).abs == (r[i+1].ord-r[i].ord).abs
      ctr+=1
    end
  end
  if ctr == s.length-1
    return "Funny"
  else
    return "Not Funny"
  end
end

q = gets.strip.to_i
for a0 in (0..q-1)
  s = gets.strip
  result = funnyString(s)
  puts result;
end

