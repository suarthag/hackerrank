#!/bin/ruby

def minSteps(n, b)
  ctr = 0
  for i in 1..b.length-2
    if b[i-1..i+1] == '010'
      # p "i=#{i} text = #{b}"
      b[i+1]='1'
      ctr += 1
    end
  end
  ctr
end

n = gets.strip.to_i
b = gets.strip
result = minSteps(n, b)
puts result;

