#!/bin/ruby

def changed_letter(s)
  changed_count = 0
  sos_arr=['S','O','S']
  for i in (0..s.length-1)
    if sos_arr[i%3] != s[i]
      changed_count += 1
    end
  end
  changed_count
end

S = gets.strip
p changed_letter(S)
