#!/bin/ruby

def alternatingCharacters(s)
  check_now = 0
  delete_count = 0
  for i in (1..s.length-1)
    if s[i]==s[check_now]
      delete_count +=1
    else
      check_now = i
    end
  end
  delete_count
  # Complete this function
end

q = gets.strip.to_i
for a0 in (0..q-1)
  s = gets.strip
  result = alternatingCharacters(s)
  puts result;
end

