#!/bin/ruby

def gemstones(arr)
  result = arr[0] & arr[1]
  for i in 2..arr.length-1
    result = result & arr[i]
  end
  result.count
end

n = gets.strip.to_i
arr = Array.new(n)
for arr_i in (0..n-1)
  arr[arr_i] = gets.strip.split('')
end
result = gemstones(arr)
puts result;
