#!/bin/ruby

def palindromeIndex(s)
  if palindrome?(s)
    return -1
  end
  for i in (0..(s.length/2)-1)
    if s[i]!=s[s.length-i-1]
      # remove left
      tmp = s.dup
      tmp.slice!(i)
      if palindrome?(tmp)
        return i
      else
        tmp = s.dup
        tmp.slice!(s.length-i-1)
        if palindrome?(tmp)
          return s.length-i-1
        end
      end
    end

  end
end
def palindrome?(s)
  reverse = s.reverse
  # p "s:#{s} rev #{reverse}"
  if reverse == s
    return true
  else
    return false
  end
end

q = gets.strip.to_i
for a0 in (0..q-1)
  s = gets.strip
  result = palindromeIndex(s)
  puts result;
end

