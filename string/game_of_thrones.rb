#!/bin/ruby

def gameOfThrones(s)
  letter=Hash.new(0)

  for i in (0..s.length-1)
    letter[s[i]] += 1
  end
  odd_number_counter = 0
  letter.each_value do |count|
    if count % 2 == 1
      odd_number_counter += 1
    end
  end
  if odd_number_counter>1
    return 'NO'
  else
    return 'YES'
  end
  # p letter
end

s = gets.strip
result = gameOfThrones(s)
puts result;

