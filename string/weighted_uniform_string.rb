#!/bin/ruby
require 'set'
def uniform_string(s,arr)
  starting_index = 0
  set_weight = Set.new
  for i in (0..s.length-1)
    if s[i] != s[starting_index]
      starting_index = i
    end
    weight = (s[i].ord.-96)*((i-starting_index)+1)
    set_weight.add(weight)
  end

  arr.each do |query|
    if set_weight.include?(query)
      puts 'Yes'
    else
      puts 'No'
    end
  end
end

s = gets.strip
n = gets.strip.to_i
arr = []
for a0 in (0..n-1)
  x = gets.strip.to_i
  arr.push(x)
end
uniform_string(s,arr)