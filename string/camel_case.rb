#!/bin/ruby

def count_words(s)
  count = 1
  for i in (0..s.length-1)
    if /[[:upper:]]/.match(s[i])
      count += 1
    end
  end
  count
end

s = gets.strip
p count_words(s)
