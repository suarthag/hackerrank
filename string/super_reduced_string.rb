#!/bin/ruby



def super_reduced_string(s)
  same_position = reducable(s)
  while same_position >= 0
    s.slice!(same_position+1)
    s.slice!(same_position)
    same_position = reducable(s)
  end
  s
end

def reducable(s)
  for i in (0..s.length-2)
    if (s[i] == s[i+1])
      return i
    end
  end
  return -1
end

s = gets.strip
result = super_reduced_string(s)
result = result.empty? ? 'Empty String' : result
puts result;

