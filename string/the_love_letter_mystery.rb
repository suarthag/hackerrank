#!/bin/ruby

def theLoveLetterMystery(s)
  # Complete this function
  ctr=0
  for i in (0..(s.length/2)-1)
     ctr = ctr+(s[i].ord - s[s.length-1-i].ord).abs
  end
  ctr
end

q = gets.strip.to_i
for a0 in (0..q-1)
  s = gets.strip
  result = theLoveLetterMystery(s)
  puts result;
end

