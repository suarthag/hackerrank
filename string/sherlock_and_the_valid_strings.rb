#!/bin/ruby

def isValid(s)
  word_counter = Hash.new(0)
  ctr = Hash.new(0)

  s.each_char do |word|
    word_counter[word] += 1
  end

  word_counter.each_value do |count|
    ctr[count] += 1
  end
  #
  # p word_counter
  # p ctr

  if ctr.count>2
    return 'NO'
  elsif ctr.count == 1
    return 'YES'
  else
    keys=ctr.keys
    if ((keys[1]*ctr[keys[1]])).abs == 1 || (keys[0]-(keys[1]*ctr[keys[1]])).abs == 1
      return 'YES'
    else
      return 'NO'
    end
  end

end

s = gets.strip
result = isValid(s)
puts result;

