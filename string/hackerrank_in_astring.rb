#!/bin/ruby

def hackerrank(s)
  hackerrank_string = 'hackerrank'
  hackerrank_ctr = 0
  for i in (0..s.length-1)
    if s[i]==hackerrank_string[hackerrank_ctr]
      hackerrank_ctr += 1
    end
  end
  if hackerrank_ctr == hackerrank_string.length
    return 'YES'
  else
    return 'NO'
  end
end

q = gets.strip.to_i
for a0 in (0..q-1)
  s = gets.strip
  puts hackerrank(s)
end

