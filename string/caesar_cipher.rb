#!/bin/ruby

def cipher(s,k)
  result = ''
  k=k%26
  s.each_byte do |c|
    if (c>=65 && c<=90) || (c>=97 && c<=122)
      encrypted = (c+k)
      if encrypted>122 && c>=97
        encrypted = (encrypted%122)+96
      elsif encrypted >90 && c<=90
        encrypted = (encrypted%90)+64
      end
    else
      encrypted = c
    end
    result = "#{result}#{encrypted.chr}"
  end
  result
end


n = gets.strip.to_i
s = gets.strip
k = gets.strip.to_i
print cipher(s,k)
