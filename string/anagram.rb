#!/bin/ruby

def anagram(s)
  if s.length%2==1
    return -1
  end
  half = s.length/2
  s1 = s1=s[0..half-1]
  s2=s[half..s.length-1]

  for i in (0..s1.length-1)
    index = s2.index(s1[i])
    if !index.nil?
      s2.slice!(index)
    end
  end
  return s2.length
end

q = gets.strip.to_i
for a0 in (0..q-1)
  s = gets.strip
  result = anagram(s)
  puts result;
end


