#!/bin/ruby

def twoStrings(s1, s2)
  s1 = s1.split('')
  s2 = s2.split('')
  result = s1 & s2
  if result.count > 0
    'YES'
  else
    'NO'
  end
end

q = gets.strip.to_i
for a0 in (0..q-1)
  s1 = gets.strip
  s2 = gets.strip
  result = twoStrings(s1, s2)
  puts result;
end

