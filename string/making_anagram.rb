#!/bin/ruby

def makingAnagrams(s1, s2)
  ctr = 0
  for i in (0..s1.length-1)
    index = s2.index(s1[i])
    # p index
    if !index.nil?
      s2.slice!(index)
    else
      ctr+=1
    end
  end
  return ctr+s2.length
end

s1 = gets.strip
s2 = gets.strip
result = makingAnagrams(s1, s2)
puts result;

