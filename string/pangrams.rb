
def pangram(s)
  alphabet = ''
  for i in ('a'..'z')
    alphabet = alphabet+i
  end
  for i in (0..s.length-1)
    alphabet = alphabet.delete(s[i])
  end
  if alphabet.length == 0
    return 'pangram'
  else
    return 'not pangram'
  end
end

S = gets.strip.downcase
puts pangram(S)
