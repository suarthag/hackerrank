#!/bin/ruby

def minimumAbsoluteDifference(n, arr)
  min = 100000
  arr = arr.sort
    for i in (0..arr.count-2)
      j = i+1
      if i!=j
        diff = (arr[i]-arr[j]).abs
        if (diff<min)
          min = diff
        end
      end
    end
  min
end

n = gets.strip.to_i
arr = gets.strip
arr = arr.split(' ').map(&:to_i)
result = minimumAbsoluteDifference(n, arr)
puts result

