#!/bin/ruby

def min_walk(calories)
  sorted_ary = calories.sort.reverse
  total = 0
  for i in (0..sorted_ary.count-1)
    total += (sorted_ary[i]*(2**i))
  end
  total
end

n = gets.strip.to_i
calories = gets.strip
calories = calories.split(' ').map(&:to_i)
p min_walk(calories)

