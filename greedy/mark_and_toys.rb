#!/bin/ruby

def maximumToys(prices, k)
  prices = prices.sort
  total_price = 0
  toys_count = 0
  prices.each do |price|
    if total_price+price < k
      toys_count += 1
      total_price += price
    end
  end
  toys_count
end

n, k = gets.strip.split(' ')
n = n.to_i
k = k.to_i
prices = gets.strip
prices = prices.split(' ').map(&:to_i)
result = maximumToys(prices, k)
puts result

