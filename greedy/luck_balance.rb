
def luck_balance(k,important_competition,unimportant_competition)
  important_competition = important_competition.sort
  lose = important_competition.count - k
  minus_luck = 0
  plus_luc = 0
  if lose < 0
    lose = 0
  end
  for i in (0..lose-1)
    minus_luck += important_competition[i]
  end
  for j in (lose..important_competition.count-1)
    plus_luc += important_competition[j]
  end
  unimportant_competition.each do |luck_point|
    plus_luc += luck_point
  end
  result = plus_luc - minus_luck
  result
end

n, k = gets.strip.split(' ')
n = n.to_i
k = k.to_i
important_competition = []
unimportant_competition = []

for i in (0..n-1)
  luck, important_index = gets.strip.split(' ')
  if important_index == '1'
    important_competition.push(luck.to_i)
  else
    unimportant_competition.push(luck.to_i)
  end
end
p luck_balance(k,important_competition,unimportant_competition)