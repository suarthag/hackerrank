#!/bin/ruby

def decent_number(arr)
  result = []
  arr.each do |number|
    decent = property4(number)
    result.push(decent.empty? ? -1 : decent)
  end
  result
end

def property4(number)
  result = ''
  five_counter = 0
  while number>0
    if number % 3 == 0
      result+= '5'* ((number/3)*3) + '3'*five_counter*5
      return result
    else
      number -= 5
      five_counter += 1
    end
  end
  if number == 0
    result+= '3'*(five_counter*5)
  end
  result
end

t = gets.strip.to_i
arr = []
for a0 in (0..t-1)
  n = gets.strip.to_i
  arr.push(n)
end

puts decent_number(arr)

