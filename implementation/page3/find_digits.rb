#!/bin/ruby

def find_digits(n)
  counter = 0
  n_array = n.to_s.split('').map(&:to_i)
  n_array.each do |number|
    if number != 0 && n % number == 0
      counter += 1
    end
  end
  counter
end

result = []
t = gets.strip.to_i
for a0 in (0..t-1)
  n = gets.strip.to_i
  result.push(find_digits(n))
end
puts result

