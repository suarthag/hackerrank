#!/bin/ruby

def saveThePrisoner(n, m, s)
  position = (s+(m-1)) % n
  position == 0 ? n : position
end

t = gets.strip.to_i
result = []
for a0 in (0..t-1)
  n, m, s = gets.strip.split(' ')
  n = n.to_i
  m = m.to_i
  s = s.to_i
  result.push(saveThePrisoner(n, m, s))

end
puts result;
