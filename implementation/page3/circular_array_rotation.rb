#!/bin/ruby

def rotate_array(arr,times)
  rotated_array = []
  for i in (0..arr.count-1)
    rotated_array[(i+times)%arr.count] = arr[i]
  end
  rotated_array
end

n,k,q = gets.strip.split(' ')
n = n.to_i
k = k.to_i
q = q.to_i
a = gets.strip
a = a.split(' ').map(&:to_i)
result = []
rotated = rotate_array(a,k)

for a0 in (0..q-1)
  m = gets.strip.to_i
  result.push(rotated[m])
end

puts result


