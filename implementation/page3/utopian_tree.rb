#!/bin/ruby


def treeHeightAtCycle(n)
  treeHeight = 1
  for i in (1..n)
    if i % 2 == 1
      treeHeight *= 2
    else
      treeHeight += 1
    end
  end
  treeHeight
end

t = gets.strip.to_i
result = []
for a0 in (0..t-1)
  n = gets.strip.to_i
  result.push(treeHeightAtCycle(n))
end
puts result

