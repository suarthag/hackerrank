#!/bin/ruby

def count_likes_at_day(n)
  start = 2
  likes = start

  for i in (2..n)
    start = (start*3) / 2
    likes += start
  end
  likes
end
n = gets.strip.to_i
p count_likes_at_day(n)