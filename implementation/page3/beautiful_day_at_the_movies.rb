#!/bin/ruby


def beautiful_day(i,j,k)
  beautiful_day_counter = 0
  for day in (i..j)
    reverseDay = reverse(day)
    if (day-reverseDay).abs % k == 0
      beautiful_day_counter += 1
    end
  end
  beautiful_day_counter
end

def reverse(n)
  n=n.to_s
  n=n.reverse
  n=n.to_i
end

i,j,k = gets.strip.split(' ')
i = i.to_i
j = j.to_i
k = k.to_i
p beautiful_day(i,j,k)