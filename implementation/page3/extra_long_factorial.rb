#!/bin/ruby

def factorial(n)
  result = 1
  while n > 1 do
    result *= n
    n -= 1
  end
  result
end

n = gets.strip.to_i
p factorial(n)
