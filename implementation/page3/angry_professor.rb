#!/bin/ruby

def check_attendance(treshold,arrival_times)
  onTimeStudent = 0
  arrival_times.each do |arrival_time|
    if arrival_time <= 0
      onTimeStudent += 1
    end
  end
  onTimeStudent < treshold ? 'YES' : 'NO'
end

t = gets.strip.to_i
result = []
for a0 in (0..t-1)
  n,k = gets.strip.split(' ')
  n = n.to_i
  k = k.to_i
  a = gets.strip
  a = a.split(' ').map(&:to_i)
  result.push(check_attendance(k,a))
end
puts result

