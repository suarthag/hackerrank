#!/bin/ruby

def pp_function(arr)
  result = []
  for i in (1..arr.count)
    index1 = arr.find_index(i)+1
    index2 = arr.find_index(index1)+1
    result.push(index2)
  end
  result
end

n = gets.strip.to_i
a = gets.strip
arr = a.split(' ').map(&:to_i)

puts pp_function(arr)