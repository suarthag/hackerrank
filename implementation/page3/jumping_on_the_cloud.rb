#!/bin/ruby

def count_energy(k,c)
  position = 0
  energy = 100
  begin
    if c[position] == 1
      energy -= 2
    end
    position = (position+k) % c.count
    energy -= 1
  end until position == 0
  energy
end

n,k = gets.strip.split(' ')
n = n.to_i
k = k.to_i
c = gets.strip
c = c.split(' ').map(&:to_i)

p count_energy(k,c)