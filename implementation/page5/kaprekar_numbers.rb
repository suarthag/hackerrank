
def kaprekar_in(p,q)
  result = []
  if p == 1
    result.push (1)
  end

  for i in (p..q)
    if kaprekar?(i)
      result.push(i)
    end
  end

  if result.count == 0
    'INVALID RANGE'
  else
    result.join(' ')
  end
end

def kaprekar?(number)
  number_square = number**2
  number_string = number_square.to_s
  separator = number_string.length/2
  l = number_string[0..separator-1]
  r = number_string[separator..number_string.length-1]
  if l.to_i+r.to_i == number
    true
  else
    false
  end
end
p = gets.strip.to_i
q = gets.strip.to_i
print kaprekar_in(p,q)
