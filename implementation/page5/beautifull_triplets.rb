
def count_triplets(d,arr)
  triplets = 0
  arr.each do |number|
   if arr.include?(number+d) && arr.include?(number+d*2)
     triplets += 1
   end
  end
  triplets
end

n,d = gets.strip.split(' ')
d = d.to_i
arr = gets.strip
arr = arr.split(' ').map(&:to_i)

p count_triplets(d,arr)