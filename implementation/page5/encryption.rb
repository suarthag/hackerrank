#!/bin/ruby

def createTable(s)
  s = s.delete(' ')
  root_l = Math.sqrt(s.length)
  row_size = root_l.floor
  if (s.length - (row_size*row_size)>0)
    column_size = row_size+1
    if (row_size*column_size<s.length)
      row_size+=1
    end
  else
    column_size = row_size
  end

  encryption_table = Hash.new
  for i in (0..s.length-1)
    row = i / column_size
    col = i % column_size
    encryption_table[[row,col]] = s[i]
  end
  result = ""

  for i in (0..column_size-1)
    for j in (0..row_size-1)
      result = "#{result}#{encryption_table[[j,i]]}"
    end
    result = "#{result} "
  end
  result
end

s = gets.strip

print createTable(s)