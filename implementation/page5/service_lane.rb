#!/bin/ruby

def service_lane(width,segements)
  result = []
  segements.each do |segment|
    i,j = segment.split(' ').map(&:to_i)
    min = width[i..j].min
    result.push(min)
  end
  result
end


n,t = gets.strip.split(' ')
n = n.to_i
t = t.to_i
width = gets.strip
width = width.split(' ').map(&:to_i)
segments = []
for a0 in (0..t-1)
  segments.push(gets.strip)
end
puts service_lane(width,segments)

