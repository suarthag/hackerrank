#!/bin/ruby


def calculate_cost(b,w,x,y,z)

  if x+z<y
    y=x+z
  elsif y+z<x
    x=y+z
  end

  total_black_cost = b*x
  total_white_cost = w*y
  total_cost = total_black_cost+total_white_cost
  total_cost
end

result = []
t = gets.strip.to_i
for a0 in (0..t-1)
  b,w = gets.strip.split(' ')
  b = b.to_i
  w = w.to_i
  x,y,z = gets.strip.split(' ')
  x = x.to_i
  y = y.to_i
  z = z.to_i
  result.push(calculate_cost(b,w,x,y,z))
end
puts result
