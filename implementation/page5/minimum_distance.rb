#!/bin/ruby

def min_distance(a)
  result=[]
  minimum = nil
  for i in (0..a.count-1)
    couple = a[i+1..a.count-1].index(a[i])
    if !couple.nil?
      couple+=i+1
      result.push((i-couple).abs)
    end
  end
  result.min.nil? ? -1 : result.min
end

n = gets.strip.to_i
a = gets.strip
a = a.split(' ').map(&:to_i)
p min_distance(a)
