#!/bin/ruby

def chocolate_feast(trips)

  trip_results = []
  trips.each do |trip|
    n,c,m = trip.split(' ')
    n = n.to_i
    c = c.to_i
    m = m.to_i
    eat = n/c
    wrappers = eat
    bonus = 0
    while wrappers >= m do
      exchange = wrappers/m
      bonus = bonus + wrappers/m
      wrappers = wrappers-(m*exchange)+exchange
    end

    trip_results.push(eat+bonus)

  end
  trip_results
end

t = gets.strip.to_i
trips = []
for a0 in (0..t-1)
  trip = gets.strip
  trips.push(trip)
end
puts chocolate_feast(trips)

