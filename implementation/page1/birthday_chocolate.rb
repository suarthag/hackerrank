#!/bin/ruby

def solve(n, s, d, m)
  # Complete this function
  result = 0
  for i in (0..n-m)
    if d==sumFromIndex(s,i,i+m)
      result += 1
    end
  end
  result
end

def sumFromIndex(arr,startIndex,endIndex)
  sum = 0
  for i in (startIndex..endIndex-1)
    sum += arr[i].to_i
  end
  sum
end

n = gets.strip.to_i
s = gets.strip
s = s.split(' ').map(&:to_i)
d, m = gets.strip.split(' ')
d = d.to_i
m = m.to_i
result = solve(n, s, d, m)
puts result;

