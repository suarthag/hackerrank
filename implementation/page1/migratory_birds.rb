#!/bin/ruby

def migratoryBirds(n, ar)
  counterArray=Array.new(5,0)
  ar.each do |bird|
    counterArray[bird-1] += 1
  end
  max = getMaxIndex(counterArray)
  max+1
end

def getMaxIndex(arr)
  max = 0
  maxIndex = -1

  for i in (0..4)
    if arr[i]>max
      max = arr[i]
      maxIndex = i
    end
  end
  maxIndex
end

n = gets.strip.to_i
ar = gets.strip
ar = ar.split(' ').map(&:to_i)
result = migratoryBirds(n, ar)
puts result

