#!/bin/ruby

def getRecord(s)
  # Complete this function
  bestScoreBrokeCount = 0
  worstScoreBrokeCount = 0

  if s.count > 0
    bestRecord=s.first
    worstRecord = s.first


    s.each do |score|
      if (score>bestRecord)
        bestRecord = score
        bestScoreBrokeCount += 1
      elsif (score < worstRecord)
        worstRecord = score
        worstScoreBrokeCount += 1
      end
    end
  end
  [bestScoreBrokeCount,worstScoreBrokeCount]

end

n = gets.strip.to_i
s = gets.strip
s = s.split(' ').map(&:to_i)
result = getRecord(s)
print result.join(" ")

