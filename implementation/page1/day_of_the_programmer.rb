#!/bin/ruby

def solve(year)
  # Complete this function
  if year == 1918
    return '26.09.1918'
  elsif leapYear?(year)
    "12.09.#{year}"
  else
    "13.09.#{year}"
  end
end

def leapYear?(year)
  if year>=1918
    leapYearGregorian?(year)
  else
    leapYearJulian?(year)
  end
end

def leapYearJulian?(year)
  year % 4 == 0
end

def leapYearGregorian?(year)
  (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)
end

year = gets.strip.to_i
result = solve(year)
puts result;


