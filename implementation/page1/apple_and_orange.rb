#!/bin/ruby

s,t = gets.strip.split(' ')
s = s.to_i
t = t.to_i
a,b = gets.strip.split(' ')
a = a.to_i
b = b.to_i
m,n = gets.strip.split(' ')
m = m.to_i
n = n.to_i
apple = gets.strip
apple = apple.split(' ').map(&:to_i)
orange = gets.strip
orange = orange.split(' ').map(&:to_i)

def countAppleOrangeInHouse(appleTreeLocation, orangeTreeLocation, houseLocationS, houseLocationT, apples, oranges)
  appleCount = countFruitInHouse(apples,appleTreeLocation,houseLocationS,houseLocationT)
  orangeCount = countFruitInHouse(oranges,orangeTreeLocation,houseLocationS,houseLocationT)

  [appleCount,orangeCount]
end

def countFruitInHouse(fruitDistances,treeLocation,houseLocationS,houseLocationT)
  fruitCount = 0
  fruitDistances.each do |fruitDistance|
    fruitLocation = calculatePositionFromTree(treeLocation,fruitDistance)
    if (fallWithinHouse(houseLocationS,houseLocationT,fruitLocation))
      fruitCount += 1
    end
  end
  fruitCount
end

def calculatePositionFromTree(treeLocation,distance)
  treeLocation + distance
end

def fallWithinHouse(point1,point2,fruit)
  return (point1<=fruit && point2>=fruit)
end


result = countAppleOrangeInHouse(a, b, s, t, apple, orange)
p result[0]
p result[1]