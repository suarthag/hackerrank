#!/bin/ruby

def solve(grades)
  # Complete this function
  result=[]
  grades.each do |grade|
    difWithMultipleOfFive = 5-(grade % 5)
    if (grade>=38 && difWithMultipleOfFive < 3)
      grade = grade+difWithMultipleOfFive
    end
    result.push(grade)
  end
  result
end

n = gets.strip.to_i
grades = Array.new(n)
for grades_i in (0..n-1)
  grades[grades_i] = gets.strip.to_i
end
result = solve(grades)
print result.join("\n")



