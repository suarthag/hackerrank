#!/bin/ruby

def bonAppetit(n, k, b, ar)
  totalSharedItem = costSharedItem(k,ar)
  if totalSharedItem/2 == b
    'Bon Appetit'
  else
    b - (totalSharedItem/2)
  end
end


def costSharedItem(k,ar)
  total_cost=0
  for i in (0..ar.count-1)
    if i != k
      total_cost += ar[i]
    end
  end
  total_cost
end

n, k = gets.strip.split(' ')
n = n.to_i
k = k.to_i
ar = gets.strip
ar = ar.split(' ').map(&:to_i)
b = gets.strip.to_i
result = bonAppetit(n, k, b, ar)
puts result;

