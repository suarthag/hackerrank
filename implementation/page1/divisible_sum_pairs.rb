#!/bin/ruby

def divisibleSumPairs(n, k, ar)
  # Complete this function
  counter = 0
  for i in (0..n-2)
    for j in (i+1..n-1)
      if ((ar[i]+ar[j]) % k ==0)
        counter +=1
      end
    end
  end
  counter
end

n, k = gets.strip.split(' ')
n = n.to_i
k = k.to_i
ar = gets.strip
ar = ar.split(' ').map(&:to_i)
result = divisibleSumPairs(n, k, ar)
puts result;

