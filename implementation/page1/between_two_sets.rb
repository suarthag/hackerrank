#!/bin/ruby

def getTotalX(a, b)
  # Complete this function
  result=[]
  for candidateX in (a.max..b.min)
    if factorOfX(candidateX,a) && factorOfArray(candidateX,b)
      result.push(candidateX)
    end
  end
  result.count
end

def factorOfX(x,arr)
  counter = 0
  arr.each do |number|
    if (x % number == 0)
      counter += 1
    end
  end
  arr.count == counter
end

def factorOfArray(x,arr)
  counter = 0
  arr.each do |number|
    if (number % x == 0)
      counter += 1
    end
  end
  arr.count == counter
end

n, m = gets.strip.split(' ')
n = n.to_i
m = m.to_i
a = gets.strip
a = a.split(' ').map(&:to_i)
b = gets.strip
b = b.split(' ').map(&:to_i)
total = getTotalX(a, b)
puts total

