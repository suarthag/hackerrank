#!/bin/ruby

def cut_the_stick(arr)
  result = [arr.count]
  while arr.count > 0 do
    min = arr.min
    arr = cut_all(arr,min)
    if arr.count>0
      result.push(arr.count)
    end
  end
  result
end

def cut_all(arr,cut_amount)
  cut_result=[]
  arr.each do |stick|
    if stick != cut_amount
      cut_result.push(stick-cut_amount)
    end
  end
  cut_result
end

n = gets.strip.to_i
arr = gets.strip
arr = arr.split(' ').map(&:to_i)
puts cut_the_stick(arr)

