#!/bin/ruby

def count_team(m,topic)
  result_counter = Hash.new(0)
  for i in (0..topic.count-2)
    for j in (i+1..topic.count-1)
      topic_count = count_topic(topic[i],topic[j])
      result_counter[topic_count] += 1
      # p "i: #{i} j:#{j} #{topic_count}"
    end
  end


  max_topic = 0
  result_counter.each_key do |key|
    if key>max_topic
      max_topic = key
    end
  end
  max_topic_counter = result_counter[max_topic]

  [max_topic,max_topic_counter]
end

def count_topic(member1,member2)
  topic = 0
  bit1 = member1.to_i(2)
  bit2 = member2.to_i(2)
  result = bit1|bit2
  result = result.to_s(2)
  result.count('1')
end

n,m = gets.strip.split(' ')
n = n.to_i
m = m.to_i
topic = Array.new(n)
for topic_i in (0..n-1)
  topic[topic_i] = gets.strip
end
puts count_team(m,topic)

