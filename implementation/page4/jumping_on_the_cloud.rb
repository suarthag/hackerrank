#!/bin/ruby

def count_step(c)
  current_position = 0
  step = 0
  while current_position != c.count-1 do
    if (c[current_position+2]== 0 && current_position+2<c.count)
      step +=1
      current_position += 2

    elsif (current_position+1<c.count)
      step+=1
      current_position += 1
    end
  end
  step
end



n = gets.strip.to_i
c = gets.strip
c = c.split(' ').map(&:to_i)
p count_step(c)
