#!/bin/ruby

def count_a(s,n)
  remainder = n % s.length
  # p remainder
  a_count_in_sentence = 0
  plus_a = 0
  for i in (0..s.length)
    if s[i] == 'a'
      a_count_in_sentence += 1
      if i<remainder
        plus_a+=1
      end
    end
  end
  total_a = ((n / s.length)*a_count_in_sentence )+plus_a
  return total_a
end

s = gets.strip
n = gets.strip.to_i
p count_a(s,n)

