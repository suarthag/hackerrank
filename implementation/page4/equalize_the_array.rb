

def equalize(arr)
  counter = Hash.new(0)
  arr.each do |number|
    counter[number] += 1
  end
  max_equal = 0
  counter.each_value do |value|
    if value>max_equal
      max_equal = value
    end
  end
  arr.count - max_equal
end

n = gets.strip.to_i
arr = gets.strip
arr = arr.split(' ').map(&:to_i)
p equalize(arr)
