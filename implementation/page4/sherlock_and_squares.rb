

def count_squares(a,b)
  lowerBound = Math.sqrt(a)
  upperBound = Math.sqrt(b)
  lowerBound = lowerBound.ceil
  upperBound = upperBound.floor

  count = (upperBound - lowerBound)+1
  count
end

t = gets.strip.to_i
result = []
for a0 in (0..t-1)
  a, b = gets.strip.split(' ')
  a = a.to_i
  b = b.to_i
  result.push(count_squares(a,b))
end
puts result
