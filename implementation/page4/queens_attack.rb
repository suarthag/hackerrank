#!/bin/ruby

def calculate_square(n,rQueen,cQueen,obstacles)
  direction = Hash.new(0)
  direction["north"] = rQueen - 1
  direction["south"] = n - rQueen
  direction["east"] = n - cQueen
  direction["west"] = cQueen - 1
  if cQueen<=rQueen
    direction["northwest"] = direction["west"]
    direction["northeast"] = direction["east"]
    direction["southwest"] = direction["south"]
    direction["southeast"] = direction["south"]
  elsif cQueen>rQueen
    direction["northwest"] = direction["north"]
    direction["northeast"] = direction["north"]
    direction["southwest"] = direction["west"]
    direction["southeast"] = direction["east"]
  end
   p direction
  obstacles.each do |obstacle|
    rObs,cObs = obstacle.split(' ')
    rObs = rObs.to_i
    cObs = cObs.to_i
    rowDistance = rQueen -  rObs
    colDistance = cQueen - cObs
    p "queen= #{rQueen},#{cQueen}  obs= #{rObs},#{cObs}"
    if (rowDistance > 0 && colDistance==0)
      if rowDistance.abs - 1 < direction["north"]
        direction["north"] = rowDistance.abs - 1
      end
       p "north"
    elsif (rowDistance < 0 && colDistance == 0)
      if rowDistance - 1 < direction["south"]
        direction["south"] = rowDistance - 1
      end
      p "south"
    elsif (rowDistance == 0 && colDistance < 0)
      if colDistance.abs - 1 < direction["east"]
        direction["east"] = colDistance.abs - 1
      end
      p "east -> distance: #{rowDistance}"
    elsif (rowDistance == 0 && colDistance > 0)
      if colDistance - 1< direction["west"]
        direction["west"] = colDistance - 1
      end
      p "west"
    elsif (rowDistance > 0 && colDistance < 0 && (rowDistance.abs == colDistance.abs))
      if rowDistance - 1 < direction["northeast"]
        direction["northeast"] = rowDistance - 1
      end
      p "NE"
    elsif (rowDistance > 0 && colDistance > 0 && (rowDistance.abs == colDistance.abs))
      if colDistance-1<direction["northwest"]
        direction["northwest"] = rowDistance-1
      end
      p "NW"
    elsif (rowDistance < 0 && colDistance > 0 && (rowDistance.abs == colDistance.abs))
      if rowDistance.abs-1 < direction["southwest"]
        direction["southwest"] = rowDistance.abs-1
      end
      p "SW"
    elsif (rowDistance < 0 && colDistance < 0 && (rowDistance.abs == colDistance.abs))
      if rowDistance.abs < direction["southeast"]
        direction["southeast"] = rowDistance.abs
      end
      p "SE"
    end
  end

  # p direction
  total = 0
  direction.each_value do |value|
    total += value
  end
  total
end


n,k = gets.strip.split(' ')
n = n.to_i
k = k.to_i
rQueen,cQueen = gets.strip.split(' ')
rQueen = rQueen.to_i
cQueen = cQueen.to_i
obstacles = []
for a0 in (0..k-1)
  obstacle = gets.strip
  obstacles.push(obstacle)
end

p calculate_square(n,rQueen,cQueen,obstacles)