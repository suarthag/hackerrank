#!/bin/ruby

def calculate_fine(d1,m1,y1,d2,m2,y2)
  total_days1 = (y1*372)+(m1*31)+(d1)
  total_days2 = (y2*372)+(m2*31)+(d2)
  if (total_days1<total_days2)
    fine = 0
  elsif  (m1==m2) && (y1==y2)
    fine = (d1-d2)*15
  elsif (y1==y2)
    fine = 500*(m1-m2)
  else
    fine = 10000
  end
  fine

end

d1,m1,y1 = gets.strip.split(' ')
d1 = d1.to_i
m1 = m1.to_i
y1 = y1.to_i
d2,m2,y2 = gets.strip.split(' ')
d2 = d2.to_i
m2 = m2.to_i
y2 = y2.to_i
p calculate_fine(d1,m1,y1,d2,m2,y2)
