def count_subset(arr,k)
  remainder_arr = Array.new(k,0)
  if k == 1 or k == 0
    return 1
  end

  for i in (0..arr.count-1)
    remainder = arr[i]%k
    remainder_arr[remainder] += 1
  end

  if remainder_arr[0]>0
    total = 1
  else
    total = 0
  end

  for i in (1..k/2)
    if i*2==k
      total+=1
    elsif remainder_arr[i]>remainder_arr[k-i]
        total+=remainder_arr[i]
    else
      total+=remainder_arr[k-i]
    end
  end


  total

end

n,k = gets.strip.split(' ')
n = n.to_i
k = k.to_i
arr = gets.strip.split(' ').map(&:to_i)
p count_subset(arr,k)
