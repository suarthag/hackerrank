#!/bin/ruby

def fair_rations(breads)
  total = 0
  breads.each do |bread|
    total += bread
  end
  if total % 2 == 1
    return 'NO'
  end
  counter = 0
  for i in (0..breads.count-1)
    if breads[i] % 2 == 1
      breads[i]+=1
      breads[i+1]+=1
      counter += 2
    end
  end
  return counter.to_s
end

N = gets.strip.to_i
input = gets.strip
B = input.split(' ').map(&:to_i)
print fair_rations(B)
