#!/bin/ruby

def get_cavity(n,grid)
  result = grid.dup
  for i in (1..n-2)
    for j in (1..n-2)
      if grid[i][j]>grid[i-1][j] &&
          grid[i][j]>grid[i+1][j] &&
          grid[i][j]>grid[i][j-1] &&
          grid[i][j]>grid[i][j+1]
        result[i][j] = 'X'
      end
    end
  end
  result.each do |row|
    puts row.join('')
  end
  result
end

n = gets.strip.to_i
grid = Array.new(n)
for grid_i in (0..n-1)
  grid[grid_i] = gets.strip.split('')
end
get_cavity(n,grid)
