#!/bin/ruby

def max_distance(n,c)
  c = c.sort
  max_distance = c[0]
  for i in (0..c.count-2)
    distance = (c[i+1]-c[i])/2
    if distance > max_distance
      max_distance = distance
    end
  end
  if n-c[c.count-1] > max_distance
    max_distance = n-c[c.count-1]-1
  end
  max_distance
end

n,m = gets.strip.split(' ')
n = n.to_i
m = m.to_i
c = gets.strip
c = c.split(' ').map(&:to_i)
p max_distance(n,c)

