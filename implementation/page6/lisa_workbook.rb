

def special_pages(k,q)
  pages = 1
  special_pages_counter = 0
  q.each do |number_of_problem|
    problem_counter = 0
    while problem_counter < number_of_problem
      if (number_of_problem-problem_counter) > k
        if (pages>problem_counter) && (pages<=problem_counter+k)
          special_pages_counter += 1
        end
      else
        if (pages>problem_counter) && (pages<=problem_counter+(number_of_problem-problem_counter))
          special_pages_counter += 1
        end
      end
      problem_counter += k
      pages+=1
    end
  end
  special_pages_counter
end

n,k = gets.strip.split(' ')
n = n.to_i
k = k.to_i
q = gets.strip.split(' ').map(&:to_i)
p special_pages(k,q)
