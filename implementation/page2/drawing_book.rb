#!/bin/ruby

def solve(n, p)
  # Complete this function
  fromPage1 = (p/2)
  fromLast = (n/2)-(p/2)
  fromPage1<fromLast ? fromPage1 : fromLast
end

n = gets.strip.to_i
p = gets.strip.to_i
result = solve(n, p)
puts result;

