#!/bin/ruby

def getSize(h,word)
  size_map = Hash.new
  index=0
  for i in ('a'..'z')
    size_map[i]=h[index]
    index+=1
  end
  max_height = 0
  word.each do |alphabet|
    if size_map[alphabet] > max_height
      max_height = size_map[alphabet]
    end
  end
  max_height*word.count
end

h = gets.strip
h = h.split(' ').map(&:to_i)
word = gets.strip.split('').map(&:to_s)
p getSize(h,word)

