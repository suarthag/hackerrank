#!/bin/ruby

def pick_number(a)
  number_counter = Hash.new(0)
  a.each do |number|
    number_counter[number] += 1
  end
  max_total = 0
  for i in (0..99)
    total = number_counter[i]+number_counter[i+1]
    if ( total >= max_total)
      max_total = total
    end
  end
  max_total
end

n = gets.strip.to_i
a = gets.strip
a = a.split(' ').map(&:to_i)
p pick_number(a)
