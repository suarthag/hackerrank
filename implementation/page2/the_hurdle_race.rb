#!/bin/ruby

def magic_beverages(height,k)
  max_hurdle = height.max
  max_hurdle > k ? max_hurdle - k : 0
end

n,k = gets.strip.split(' ')
n = n.to_i
k = k.to_i
height = gets.strip
height = height.split(' ').map(&:to_i)
p magic_beverages(height,k)
# your code goes here

