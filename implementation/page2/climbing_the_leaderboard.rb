#!/bin/ruby

class Champion
  attr_accessor :rank, :score
  def initialize(rank,score)
    @rank = rank
    @score = score
  end
end

def climb_leaderboard(scores,alice)
  alice_rank = []
  alice_progress = 0
  dense_ranking = dense_ranking(scores)
  leaderboard_index = scores.count - 1


  while alice_progress < alice.count
    if (leaderboard_index == 0 && dense_ranking[leaderboard_index].score < alice[alice_progress]) ||(alice_rank[alice_rank.count-1] == 1)
      alice_progress+=1
      alice_rank.push(1)
    else
      if dense_ranking[leaderboard_index].score == alice[alice_progress]
        alice_progress+=1
        alice_rank.push(dense_ranking[leaderboard_index].rank)
        leaderboard_index -= 1
      elsif dense_ranking[leaderboard_index].score > alice[alice_progress]
        alice_progress+=1
        alice_rank.push(dense_ranking[leaderboard_index].rank+1)
      else
        leaderboard_index -= 1
      end
    end
  end
  alice_rank
end

def dense_ranking(scores)
  rank = 1
  score_before = scores[0]
  dense = []
  scores.each do |score|
    if score< score_before
      rank += 1
      score_before = score
    end
    dense.push(Champion.new(rank,score))
  end
  dense
end

n = gets.strip.to_i
scores = gets.strip
scores = scores.split(' ').map(&:to_i)
m = gets.strip.to_i
alice = gets.strip
alice = alice.split(' ').map(&:to_i)
puts climb_leaderboard(scores, alice)