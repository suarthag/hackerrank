#!/bin/ruby

def count_valley(n,ar)
  garry_position = 0
  garry_position_before = 0
  valley = 0
  ar.each do |step|
    if step == 'U'
      garry_position_before = garry_position
      garry_position+=1
    elsif step == 'D'
      garry_position -=1
    end
    if garry_position == 0 && garry_position_before<0
      valley+=1
    end
  end
  valley
end

n = gets.strip.to_i
ar = gets.strip
ar = ar.split('').map(&:to_s)
result = count_valley(n, ar)
p result

