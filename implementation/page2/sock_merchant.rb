#!/bin/ruby

def sockMerchant(n, ar)
  colorCounter = Hash.new
  colorCounter.default = 0
  ar.each do |item|
    colorCounter[item] += 1
  end
  coupleCount = 0
  colorCounter.each_value {|value|
    coupleCount +=value/2
  }
  coupleCount
end

n = gets.strip.to_i
ar = gets.strip
ar = ar.split(' ').map(&:to_i)
result = sockMerchant(n, ar)
puts result;
