#!/bin/ruby

def getMoneySpent(keyboards, drives, s)
  combination=[]
  keyboards.each do |keyboard|
    drives.each do |drive|
      total = keyboard+drive
      if total <= s
        combination.push(total)
      end
    end
  end
  maxBudget = combination.max
  maxBudget.nil? ? -1 : maxBudget
end

s,n,m = gets.strip.split(' ')
s = s.to_i
n = n.to_i
m = m.to_i
keyboards = gets.strip
keyboards = keyboards.split(' ').map(&:to_i)
drives = gets.strip
drives = drives.split(' ').map(&:to_i)
#  The maximum amount of money she can spend on a keyboard and USB drive, or -1 if she can't purchase both items
moneySpent = getMoneySpent(keyboards, drives, s)
puts moneySpent;

