#!/bin/ruby


def solve(x,y,z)
  cat_a_distance = (x - z).abs
  cat_b_distance = (y - z).abs

  case
    when cat_a_distance<cat_b_distance
      'Cat A'
    when cat_a_distance>cat_b_distance
      'Cat B'
    else
      'Mouse C'
  end
end


queryResult = []
q = gets.strip.to_i
for a0 in (0..q-1)
  x,y,z = gets.strip.split(' ')
  x = x.to_i
  y = y.to_i
  z = z.to_i

  queryResult.push(solve(x,y,z))
end

puts queryResult